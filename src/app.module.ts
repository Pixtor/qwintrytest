import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TodoEntity } from 'src/modules/todo/entities/todo.entity';
import { TodoModule } from 'src/modules/todo/todo.module';

@Module({
  imports: [
      TypeOrmModule.forRoot({
        type: 'mysql',
        host: 'localhost',
        port: 3306,
        username: 'root',
        password: 'root',
        database: 'qwintry',
        entities: [
          TodoEntity
        ],
        synchronize: true
      }),
      TodoModule
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
