import { TodoEntity } from 'src/modules/todo/entities/todo.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

export class TodoService {
    constructor(@InjectRepository(TodoEntity) private readonly model: Repository<TodoEntity>) {
    }

    async create(fields: TodoEntity): Promise<TodoEntity> {
        let model: TodoEntity = new TodoEntity();
        model = Object.assign(model, fields);
        console.log(model);
        return model.save();
        // return this.model.create(fields);
    }

    async find(options: any = {}): Promise<Array<TodoEntity>> {
        return this.model.find(options);
    }

    async findOne(options: any): Promise<TodoEntity> {
        return this.model.findOne(options);
    }

    async update(id: number, fields: any): Promise<TodoEntity> {
        return this.model.update(id, fields).then(() => {
            return this.model.findOne(id);
        });
    }

    async delete(id): Promise<any> {
        return this.model.delete(id);
    }

}
