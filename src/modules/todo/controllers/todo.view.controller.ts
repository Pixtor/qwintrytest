import { Controller, Get, Render } from '@nestjs/common';

@Controller('')
export class TodoViewController {

    constructor() {
    }

    @Render('view')
    @Get('')
    view() {
    }
}
