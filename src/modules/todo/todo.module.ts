import { Module } from '@nestjs/common';
import { TodoService } from 'src/modules/todo/services/todo.service';
import { TodoController } from 'src/modules/todo/controllers/todo.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TodoEntity } from 'src/modules/todo/entities/todo.entity';
import { TodoViewController } from 'src/modules/todo/controllers/todo.view.controller';

@Module({
    imports: [TypeOrmModule.forFeature([TodoEntity])],
    controllers: [TodoController, TodoViewController],
    providers: [TodoService],
    exports: [TodoService]
})

export class TodoModule {

}
