import { BadRequestException, Body, Controller, Get, Param, Post } from '@nestjs/common';
import { TodoService } from 'src/modules/todo/services/todo.service';

@Controller('api/todo')
export class TodoController {
    constructor(private service: TodoService) {

    }

    @Post('create')
    create(@Body() body: any) {
        return this.service.create(body);
    }

    @Get('list')
    getList() {
        return this.service.find();
    }

    @Post('done')
    setDone(@Body() body: any, @Param() params: any) {
        let id: number = body.id;
        if (body.id) {
            return this.service.update(id, {done: true, urgent: false});
        }
        throw new BadRequestException();
    }

    @Post('urgent')
    setUrgent(@Body() body: any) {
        let id: number = body.id;
        if (body.id) {
            return this.service.update(id, {urgent: body.urgent});
        }
        throw new BadRequestException();
    }

    @Post('remove')
    remove(@Body() body: any) {
        let id: number = body.id;
        if (body.id) {
            return this.service.delete(id);
        }
        throw new BadRequestException();
    }
}
